# Answers to exercise 2 questions

# (Joining tables - refer to web lecture 13 slides, page 21)

# A. What are the names of the students who attend COMP219?
SELECT
  student.fname,
  student.lname,
  attend.dept,
  attend.num
FROM unidb_attend AS attend, unidb_students AS student
WHERE attend.id = student.id AND attend.dept = 'comp' AND attend.num = '219';

# B. What are the names of the student reps that are not from NZ?
SELECT
  student.fname,
  student.lname,
  student.country
FROM unidb_students AS student, unidb_courses AS course
WHERE course.rep_id = student.id AND student.country NOT IN ('NZ');

# C. Where are the offices for the lecturers of 219?
SELECT
  lecturer.fname,
  lecturer.lname,
  lecturer.office,
  teach.dept,
  teach.num
FROM unidb_lecturers AS lecturer, unidb_teach AS teach
WHERE lecturer.staff_no = teach.staff_no AND teach.num = '219';

# D. What are the names of the students taught by Te Taka?

# Courses taught by Te Taka
SELECT
  lecturer.staff_no,
  lecturer.fname,
  lecturer.lname,
  teach.dept,
  teach.num
FROM
  unidb_lecturers AS lecturer,
  unidb_teach AS teach
WHERE
  lecturer.staff_no = teach.staff_no
  AND lecturer.staff_no = 707;

# Query students taught by Te Taka
SELECT DISTINCT
  student.id,
  student.fname,
  student.lname,
  lecturer.staff_no,
  lecturer.fname,
  lecturer.lname
FROM
  unidb_students AS student,
  unidb_attend AS attend,
  unidb_lecturers AS lecturer,
  unidb_teach AS teach
WHERE
  lecturer.staff_no = teach.staff_no
  AND lecturer.staff_no = 707
  AND attend.id = student.id;

# E. List the students and their mentors
SELECT
  student.id,
  student.fname,
  student.lname,
  student.mentor,
  mentor.fname,
  mentor.lname
FROM
  unidb_students AS student,
  unidb_students AS mentor
WHERE
  student.mentor = mentor.id;

# F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ
SELECT
  lecturer.staff_no AS id,
  lecturer.fname,
  lecturer.lname,
  lecturer.office AS location
FROM
  unidb_lecturers AS lecturer
WHERE
  lecturer.office LIKE 'G.%'
UNION
SELECT
  student.id As id,
  student.fname,
  student.lname,
  student.country AS location
FROM
  unidb_students AS student
WHERE
  student.country NOT IN ('NZ');

# G. List the course co-ordinator and student rep for COMP219
SELECT
  course.dept,
  course.num,
  course.coord_no,
  coordinator.fname,
  coordinator.lname,
  course.rep_id,
  rep.fname,
  rep.lname
FROM
  unidb_courses AS course,
  unidb_lecturers AS coordinator,
  unidb_students AS rep
WHERE
  course.coord_no = coordinator.staff_no
  AND course.rep_id = rep.id
  AND course.dept = 'comp'
  AND course.num = 219;
