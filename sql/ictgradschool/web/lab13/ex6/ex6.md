#Exercise 6

##Relational Model

###Entities
Actor (id, name, salary, country)

Film (id, name)

FilmProducer (id, name, salary)

FilmGenre (id, name)

FilmCertificate (id, rating)

###Relationships
FilmProducer_Produce_Film (FilmProducer_ID, Film_ID)

Actor_Has_Film (Actor_ID, Film_ID, Role)
> **Note**: This is the table referencing the role(s) an actor have in a film. It consist of a PRIMARY KEY (unique identifier) plus a COMPOSITE KEY, which made up of the combination of Actor_ID, Film_ID, Role

Film_Has_FilmGenre (Film_ID, FilmGenre_ID)

FilmHasFilmCertificate (Film_ID, FilmCertificate_ID)