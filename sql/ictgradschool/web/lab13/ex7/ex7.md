#Exercise 6

##Relational Model

###Entities
League (league_id, name, country)

Club (club_id, name, points, stadium_id, league_id, manager_id)

Player (player_id, first_name, last_name, age, position, nationality, club_id)

Manager (manager_id, first_name, last_name, age, nationality, club_id)

Stadium (stadium_id, name, country, city, capacity, club_id)

###Relationships
ClubHasLeague (club_id, league_id)

ClubHasManager (club_id, manager_id)

ClubHasStadium (club_id, stadium_id)

PlayerHasClub (player_id, club_id)

ManagerHasClub (manager_id, club_id)