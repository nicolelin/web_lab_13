-- LEAGUE DATABASE --

-- ER MODEL --
# Please see ER Diagram file in folder

-- RELATIONAL MODEL --
# Please see .md file in folder

-- SQL CODE --

# League #

DROP TABLE IF EXISTS web_lab13_ex07_football_league;

CREATE TABLE IF NOT EXISTS web_lab13_ex07_football_league (
  league_id VARCHAR(3) NOT NULL,
  name      VARCHAR(32),
  country   VARCHAR(32),
  PRIMARY KEY (league_id)
);

INSERT INTO web_lab13_ex07_football_league (league_id, name, country) VALUES
  (001, 'English Premier League', 'England'),
  (002, 'La Liga', 'Spain'),
  (003, 'Bundesliga', 'Germany'),
  (004, 'Serie A', 'Italy'),
  (005, 'Ligue 1', 'France'),
  (006, 'Eredivisie', 'Netherlands');

# Stadium #

DROP TABLE IF EXISTS web_lab13_ex07_football_stadium;

CREATE TABLE IF NOT EXISTS web_lab13_ex07_football_stadium (
  stadium_id VARCHAR(6) NOT NULL,
  name       VARCHAR(32),
  country    VARCHAR(32),
  city       VARCHAR(20),
  capacity   INT,
  club_id    VARCHAR(6), -- *to add foreign key reference later
  PRIMARY KEY (stadium_id)
);

INSERT INTO web_lab13_ex07_football_stadium (stadium_id, name, country, city, capacity, club_id) VALUES
  ('ESPS01', 'Santiago Bernabeu', 'Spain', 'Madrid', 81044, 'ESP101'),
  ('ESPS02', 'Camp Nou', 'Spain', 'Barcelona', 99354, 'ESP102'),
  ('ESPS03', 'Vicente Calderon', 'Spain', 'Madrid', 54907, 'ESP103'),
  ('ESPS04', 'Ramon Sanchez Pizjuan', 'Spain', 'Sevilla', 42500, 'ESP104'),
  ('ESPS05', 'Estadio de la Ceramica', 'Spain', 'Villarreal', 24890, 'ESP105'),
  ('ESPS06', 'San Mames', 'Spain', 'Bilbao', 53289, 'ESP106');

# Club #

DROP TABLE IF EXISTS web_lab13_ex07_football_club;

CREATE TABLE IF NOT EXISTS web_lab13_ex07_football_club (
  club_id    VARCHAR(6) NOT NULL,
  name       VARCHAR(32),
  points     INT(2),
  stadium_id VARCHAR(6),
  league_id  VARCHAR(3),
  manager_id VARCHAR(5), -- *to add foreign key reference later
  PRIMARY KEY (club_id),
  FOREIGN KEY (league_id) REFERENCES web_lab13_ex07_football_league (league_id),
  FOREIGN KEY (stadium_id) REFERENCES web_lab13_ex07_football_stadium (stadium_id)
);

INSERT INTO web_lab13_ex07_football_club (club_id, name, points, stadium_id, league_id, ylin183.web_lab13_ex07_football_club.manager_id) VALUES
  ('ESP101', 'Real Madrid', 84, 'ESPS01', 002, 'M3921'),
  ('ESP102', 'FC Barcelona', 84, 'ESPS02', 002, 'M3752'),
  ('ESP103', 'Atletico Madrid', 74, 'ESPS03', 002, 'M2185'),
  ('ESP104', 'Sevilla', 69, 'ESPS04', 002, 'M4123'),
  ('ESP105', 'Villarreal', 63, 'ESPS05', 002, 'M4254'),
  ('ESP106', 'Athletic Bilbao', 62, 'ESPS06', 002, 'M3100');

# Player #

DROP TABLE IF EXISTS web_lab13_ex07_football_player;

CREATE TABLE IF NOT EXISTS web_lab13_ex07_football_player (
  player_id   VARCHAR(5) NOT NULL,
  first_name  VARCHAR(20),
  last_name   VARCHAR(20),
  age         INT(3),
  position    VARCHAR(10),
  nationality VARCHAR(32),
  club_id     VARCHAR(6),
  PRIMARY KEY (player_id),
  FOREIGN KEY (club_id) REFERENCES web_lab13_ex07_football_club (club_id)
);

INSERT INTO web_lab13_ex07_football_player (player_id, first_name, last_name, age, position, nationality, club_id)
VALUES
  ('P1239', 'Lionel', 'Messi', 29, 'Forward', 'Argentina', 'ESP101'),
  ('P1104', 'Andres', 'Iniesta', 32, 'Midfield', 'Spain', 'ESP102'),
  ('P2240', 'Cristiano', 'Ronaldo', 31, 'Forward', 'Portugal', 'ESP101'),
  ('P2750', 'Luka', 'Modric', 31, 'Midfield', 'Croatia', 'ESP101'),
  ('P2102', 'Diego', 'Godin', 31, 'Defense', 'Uruguay', 'ESP103'),
  ('P2851', 'Jan', 'Oblak', 24, 'Goalkeeper', 'Slovenia', 'ESP103');

# Manager #

DROP TABLE IF EXISTS web_lab13_ex07_football_manager;

CREATE TABLE IF NOT EXISTS web_lab13_ex07_football_manager (
  manager_id  VARCHAR(5) NOT NULL,
  first_name  VARCHAR(20),
  last_name   VARCHAR(20),
  age         INT(3),
  nationality VARCHAR(32),
  club_id     VARCHAR(6),
  PRIMARY KEY (manager_id),
  FOREIGN KEY (club_id) REFERENCES web_lab13_ex07_football_club (club_id)
);

INSERT INTO web_lab13_ex07_football_manager (manager_id, first_name, last_name, age, nationality, club_id) VALUES
  ('M3921', 'Zinedine', 'Zidane', 44, 'France', 'ESP101'),
  ('M3752', 'Luis', 'Enrique', 47, 'Spain', 'ESP102'),
  ('M2185', 'Diego', 'Simeone', 47, 'Argentina', 'ESP103'),
  ('M4123', 'Jorge', 'Sampaoli', 57, 'Argentina', 'ESP104'),
  ('M4254', 'Fran', 'Escriba', 52, 'Spain', 'ESP105'),
  ('M3100', 'Ernesto', 'Valverde', 53, 'Spain', 'ESP106');

# Additional commands #

-- alter table [tablename] add foreign key
# to enforce manager_id as a foreign key in club table referencing manager_id in manager table

ALTER TABLE web_lab13_ex07_football_stadium
  ADD FOREIGN KEY (club_id) REFERENCES web_lab13_ex07_football_club (club_id);

ALTER TABLE web_lab13_ex07_football_club
  ADD FOREIGN KEY (manager_id) REFERENCES web_lab13_ex07_football_manager (manager_id);

-- drop foreign keys before dropping tables
# alter table [tablename] drop foreign key [name of foreign key in db (look up in database - this is not the reference to the relationship between the two tables]

# ALTER TABLE web_lab13_ex07_football_stadium
#   DROP FOREIGN KEY web_lab13_ex07_football_stadium_ibfk_1;
# ALTER TABLE web_lab13_ex07_football_club
#   DROP FOREIGN KEY web_lab13_ex07_football_club_ibfk_3;

-- drop all tables

# DROP TABLE IF EXISTS web_lab13_ex07_football_manager;
# DROP TABLE IF EXISTS web_lab13_ex07_football_player;
# DROP TABLE IF EXISTS web_lab13_ex07_football_club;
# DROP TABLE IF EXISTS web_lab13_ex07_football_stadium;
# DROP TABLE IF EXISTS web_lab13_ex07_football_league;