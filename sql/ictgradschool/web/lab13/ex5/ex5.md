#Exercise 5

##Relational Model

###Entities
Student (id, name, position, club, subscription_fee)

Club (id, name, sport, points)

Manager (id, name, salary)

Court (id, location, booking_status)

League (id, name, game, prize_pool)

###Relationship
Student_Book_Court (Student_ID, Booking_Status)

Club_Book_Court (Club_ID, Booking_Status)

Student_Has_Club (Student_Club, Club_ID)

Club_Has_Manager (Club_ID, Manager_ID)

Club_Has_League (Club_ID, League_ID)